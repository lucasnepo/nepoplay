package com.nepoapp.nepoplay.model
// long songID, String songTitle, String songArtist
data class Song (
    val id   : Long? = 0L,
    val title: String? = "",
    val artist: String? = "")