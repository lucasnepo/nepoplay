package com.nepoapp.nepoplay.service

import android.app.Service
import android.content.ContentUris
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.IBinder
import android.os.PowerManager
import android.provider.MediaStore
import android.util.Log
import com.nepoapp.nepoplay.model.Song


class MusicService : Service(),
    MediaPlayer.OnPreparedListener,
    MediaPlayer.OnErrorListener,
    MediaPlayer.OnCompletionListener{

    private lateinit var player : MediaPlayer
    private lateinit var songs  : ArrayList<Song>
    private var songPosition: Int = 0

    override fun onCreate() {
        super.onCreate()
        player = MediaPlayer()
        startPlayMusic()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    fun startPlayMusic(){
        player.setWakeMode(getApplicationContext(),PowerManager.PARTIAL_WAKE_LOCK)
        player.setAudioStreamType(AudioManager.STREAM_MUSIC)
        player.setOnPreparedListener(this)
        player.setOnCompletionListener(this)
        player.setOnErrorListener(this)
    }

    fun playSong(){
        val playSong = songs[songPosition]
        val currSong: Long? = playSong.id
        val trackUri: Uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currSong!!)
        try {
            player.setDataSource(applicationContext, trackUri)
        } catch (e: Exception) {
            Log.e("MUSIC_SERVICE", "Error setting data source", e)
        }
        player.prepareAsync()
    }

    fun setMusic(songs : ArrayList<Song>){
        this.songs = songs
    }

    fun setSong(positionMusic : Int){
        songPosition = positionMusic
    }

    override fun onPrepared(mp: MediaPlayer?) {
        mp?.apply { start() }
    }

    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        return true
    }

    override fun onCompletion(mp: MediaPlayer?) {

    }
}