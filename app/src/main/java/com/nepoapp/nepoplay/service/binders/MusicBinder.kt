package com.nepoapp.nepoplay.service.binders

import android.os.Binder
import com.nepoapp.nepoplay.service.MusicService

class MusicBinder : Binder(){
    fun getService() = MusicService()
}