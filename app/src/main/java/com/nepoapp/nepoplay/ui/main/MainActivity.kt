package com.nepoapp.nepoplay.ui.main

import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import androidx.compose.Observe
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nepoapp.nepoplay.R
import com.nepoapp.nepoplay.databinding.ActivityMainBinding
import com.nepoapp.nepoplay.model.Song
import com.nepoapp.nepoplay.repository.MusicDeviceRepository
import com.nepoapp.nepoplay.service.MusicService
import com.nepoapp.nepoplay.service.binders.MusicBinder
import com.nepoapp.nepoplay.ui.adapters.SongAdapter
import com.nepoapp.nepoplay.ui.viewModel.MainViewModel
import kotlinx.android.synthetic.main.content_appbar_with_collapsing.*
import kotlinx.android.synthetic.main.content_main_activity.view.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.getViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class MainActivity : AppCompatActivity(),ServiceConnection {

    private lateinit var binding : ActivityMainBinding

    private val adapterSong : SongAdapter by inject()
    private var recyclerView : RecyclerView? = null
    private lateinit var mainviewModel: MainViewModel

    private val repository : MusicDeviceRepository = MusicDeviceRepository(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()
        setupRecyclerView()
    }

    override fun onStart() {
        super.onStart()

        if (mainviewModel.playIntent == null){
            mainviewModel.playIntent = Intent(this,MusicService::class.java)
            bindService(mainviewModel.playIntent,mainviewModel.getService(), Context.BIND_AUTO_CREATE)
            startService(mainviewModel.playIntent)
        }
    }

    private fun setupRecyclerView() {
        recyclerView = binding.includeRecycler.recyclerView
        recyclerView?.layoutManager = LinearLayoutManager(this)
        recyclerView?.setHasFixedSize(true)
        mainviewModel.setRepository(repository)
        mainviewModel.fetchSongs()
        mainviewModel.getSongs().observe(this,songs)
        recyclerView?.adapter = adapterSong
    }

    private val songs = Observer<ArrayList<Song>> {
        adapterSong.setSongs(it)
    }

    private fun setupView() {
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        mainviewModel = ViewModelProviders.of(this)[MainViewModel::class.java]
        setupToolbar()
    }

    private fun setupToolbar(){
        setSupportActionBar(toolbar)
        supportActionBar?.title = "NepoPlay"
        supportActionBar?.subtitle = "Música para todos meus amigos"
    }

    override fun onServiceDisconnected(name: ComponentName?) {
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        val binderService : MusicBinder = service as MusicBinder

    }
}


