package com.nepoapp.nepoplay.ui.adapters.viewHolder

import androidx.recyclerview.widget.RecyclerView
import com.nepoapp.nepoplay.databinding.CardMusicBinding
import com.nepoapp.nepoplay.model.Song
import com.nepoapp.nepoplay.ui.adapters.listener.SongListener
import java.text.FieldPosition

class SongViewHolder(view:CardMusicBinding) : RecyclerView.ViewHolder(view.root),SongListener {
    private val view = view
    private var _position : Int = 0

    fun setViewHolder(music:Song,position: Int){
        view.music = music
        _position = position
    }

    override fun onCardClicked(): Int {
        return _position
    }

    fun getSongListener() = this

}