package com.nepoapp.nepoplay.ui.viewModel

import android.app.Application
import android.app.Service
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nepoapp.nepoplay.model.Song
import com.nepoapp.nepoplay.repository.MusicDeviceRepository
import com.nepoapp.nepoplay.service.MusicService
import com.nepoapp.nepoplay.service.binders.MusicBinder


class MainViewModel(application: Application ) : AndroidViewModel(application),ServiceConnection{
    private lateinit var songRepository : MusicDeviceRepository
    private var _songs = MutableLiveData<ArrayList<Song>>()

    private lateinit var musicService: MusicService
    var playIntent: Intent? = null
    private var musicBound = false

    fun fetchSongs(){
        val songs = songRepository.getMusicsInfo()
        _songs.postValue(songs)
    }

    fun getSongs() : LiveData<ArrayList<Song>>{
        return _songs
    }

    fun setRepository(songRepository : MusicDeviceRepository){
        this.songRepository = songRepository
    }

    fun setSong(position : Int){
        musicService.setSong(position)
        musicService.playSong()
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        musicBound = false
    }

    fun getService():ServiceConnection = this

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        val binder = service as MusicBinder
        val songs = _songs.value

        songs?.let {
            musicService = binder.getService()
            musicService.setMusic(it)
            musicBound = true
        }
    }
}