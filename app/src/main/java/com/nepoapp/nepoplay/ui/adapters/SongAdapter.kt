package com.nepoapp.nepoplay.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.nepoapp.nepoplay.R
import com.nepoapp.nepoplay.databinding.CardMusicBinding
import com.nepoapp.nepoplay.model.Song
import com.nepoapp.nepoplay.ui.adapters.listener.SongListener
import com.nepoapp.nepoplay.ui.adapters.viewHolder.SongViewHolder

class SongAdapter() : RecyclerView.Adapter<SongViewHolder>() {

    private var songs = ArrayList<Song>()
    lateinit var listener : SongListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder
      = SongViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.card_music,parent,false))

    override fun getItemCount(): Int = songs.size

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
       holder.setViewHolder(songs[position],position)
       listener = holder.getSongListener()
    }

    fun getListenerReference() = listener

    fun setSongs(songs: ArrayList<Song>){
        this.songs = songs
    }
}