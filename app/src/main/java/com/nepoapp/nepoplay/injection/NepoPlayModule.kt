package com.nepoapp.nepoplay.injection

import android.content.Context
import com.nepoapp.nepoplay.repository.MusicDeviceRepository
import com.nepoapp.nepoplay.ui.adapters.SongAdapter
import com.nepoapp.nepoplay.ui.viewModel.MainViewModel
import org.koin.dsl.module.module

val nepoPlayAndroidModule = module {

    single { this }
    single { SongAdapter() }
    factory { MusicDeviceRepository(get()) }

}
