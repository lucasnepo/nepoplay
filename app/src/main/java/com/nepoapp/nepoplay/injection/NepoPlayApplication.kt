package com.nepoapp.nepoplay.injection

import android.app.Application
import org.koin.android.ext.android.startKoin
import org.koin.core.Koin
import org.koin.standalone.KoinComponent
import org.koin.standalone.StandAloneContext.stopKoin


class NepoPlayApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(
            this, arrayListOf(nepoPlayAndroidModule))
    }

    // Isso só será chamado em emuladores
    override fun onTerminate() {
        super.onTerminate()
        stopKoin()
    }

}