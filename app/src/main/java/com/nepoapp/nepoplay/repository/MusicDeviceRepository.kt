package com.nepoapp.nepoplay.repository

import android.content.ContentResolver
import android.content.Context
import android.content.ServiceConnection
import android.net.Uri
import android.provider.MediaStore
import androidx.lifecycle.LiveData
import com.nepoapp.nepoplay.model.Song

class MusicDeviceRepository(context: Context) {
    private val context: Context = context
    private var musicResolver : ContentResolver? = null
    private var musicURI : Uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI

    fun getMusicsInfo():ArrayList<Song>?{
        musicResolver =  context.contentResolver!!
        var musicCursor = musicResolver?.query(musicURI,null,null,null,null)
        var musicOfDevice : ArrayList<Song> = ArrayList()
        musicCursor?.run {
            if (this.moveToFirst()){
                val cId = getColumnIndex(MediaStore.Audio.Media._ID)
                val cTitle = getColumnIndex(MediaStore.Audio.Media.TITLE)
                val cArtist = getColumnIndex(MediaStore.Audio.Media.ARTIST)

                do {
                    musicOfDevice.add(Song(getLong(cId), getString(cTitle),getString(cArtist)))
                }while (moveToNext())
            }

        } ?: return null
        return null
    }



}